/////////////////////////////////////////////////////////
// required
/////////////////////////////////////////////////////////
var gulp = require('gulp'),
	uglify = require('gulp-uglify'),
	minifyCss = require('gulp-minify-css'),
	compass = require('gulp-compass'),
	browserSync = require('browser-sync'),
	reload = browserSync.reload,
	rename = require('gulp-rename'),
	plumber = require('gulp-plumber');


/////////////////////////////////////////////////////////
// test task
/////////////////////////////////////////////////////////
gulp.task('test', function() {
	console.log("Hit");
});


/////////////////////////////////////////////////////////
// html task
/////////////////////////////////////////////////////////
gulp.task('html', function() {
	gulp.src('app/**/*.html')
		.pipe(reload({stream:true}));
});

/////////////////////////////////////////////////////////
// compass task
/////////////////////////////////////////////////////////
gulp.task('compass', function() {
	gulp.src('app/scss/style.scss')
		.pipe(plumber())
		.pipe(compass({
			css: 'app/css',
			sass: 'app/scss'
		}))
		.pipe(minifyCss())
		.pipe(gulp.dest('app/css/'))
		.pipe(reload({stream:true}));
});


/////////////////////////////////////////////////////////
// scripts task
/////////////////////////////////////////////////////////
gulp.task('scripts', function() {
	gulp.src(['app/js/**/*.js', '!app/js/**/*.min.js'])
		.pipe(plumber())
		.pipe(rename({suffix: '.min'}))
		.pipe(uglify())
		.pipe(gulp.dest('app/js/'))
		.pipe(reload({stream:true}));
});


/////////////////////////////////////////////////////////
// watch task
/////////////////////////////////////////////////////////
gulp.task('watch', function() {
	gulp.watch('app/scss/style.scss', ['compass']);
	gulp.watch('app/**/*.html', ['html']);
	gulp.watch('app/js/**/*.js', ['scripts']);

});


/////////////////////////////////////////////////////////
// browserSync task
/////////////////////////////////////////////////////////
gulp.task('browserSync', function() {
	browserSync({
		server:{
			baseDir: "app"
		}
	});
});


/////////////////////////////////////////////////////////
// default task
/////////////////////////////////////////////////////////
gulp.task('default', [ 'html', 'compass', 'scripts', 'browserSync', 'watch']);